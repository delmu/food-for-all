package ro.edea.entity;

import java.io.Serializable;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class BaseEntity implements Serializable {

    public abstract Long getId();

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null || !(obj instanceof BaseEntity)) return false;
        final BaseEntity dataObject = (BaseEntity) obj;

        return dataObject.getId() != null && dataObject.getId().equals(getId());
    }

    @Override
    public int hashCode() {
        return getId() != null ? getId().hashCode() : super.hashCode();
    }

}