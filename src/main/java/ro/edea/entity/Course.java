package ro.edea.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.type.BinaryType;

@Entity
@Table(name = "C_COURSES")
public class Course extends BaseEntity {
	
	@Id
	@SequenceGenerator(name = "C_COURSES_SEQUENCE", sequenceName = "C_COURSES_SEQ")
	@GeneratedValue(generator = "C_COURSES_SEQUENCE")
	@Column(name = "C_ID", insertable = false, updatable = false)
	private long id;
	
	@Column(name = "C_NAME")
	private String name;
	
	@Column(name = "C_TYPE")
	private BinaryType type;

	@Override
	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BinaryType getType() {
		return type;
	}

	public void setType(BinaryType type) {
		this.type = type;
	}
	


}
