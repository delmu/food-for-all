package ro.edea.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "U_USERS")
public class User extends BaseEntity {

	@Id
	@SequenceGenerator(name = "U_USERS_SEQUENCE", sequenceName = "U_USERS_SEQ")
	@GeneratedValue(generator = "U_USERS_SEQUENCE")
	@Column(name = "U_ID", insertable = false, updatable = false)
	private long id;

	@Column(name = "U_NAME")
	private String name;

	@Column(name = "U_EMAIL")
	private String email;

	@Override
	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
