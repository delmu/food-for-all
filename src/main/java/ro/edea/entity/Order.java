package ro.edea.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "O_ORDERS")
public class Order extends BaseEntity {

	@Id
	@SequenceGenerator(name = "O_ORDERS_SEQUENCE", sequenceName = "O_ORDERS_SEQ")
	@GeneratedValue(generator = "O_ORDERS_SEQUENCE")
	@Column(name = "O_ID", insertable = false, updatable = false)
	private long id;
	
	@Column (name = "O_U_ID")
	private long userId;
	
	
	@Override
	public Long getId() {
		return id;
	}

}