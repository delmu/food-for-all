package ro.edea;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/courses")
public class CoursesController {
	
	@RequestMapping(value="/{fcId}/{scId}/{dId}" , method=RequestMethod.GET)
	public String order(
			@PathVariable Integer fcId,
			@PathVariable Integer scId,
			@PathVariable Integer dId){
		
		return fcId.toString() + " " + scId.toString() + " " + dId.toString() ;
	}
}
